<?php


namespace TiktokShop\Resources;

use TiktokShop\Resource;

class Authorization extends Resource
{
    protected $category = 'authorization';

    public function getAuthorizedShop()
    {
        return $this->call('GET', 'shops');
    }
}
