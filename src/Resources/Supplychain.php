<?php


namespace TiktokShop\Resources;

use GuzzleHttp\RequestOptions;
use TiktokShop\Resource;

class Supplychain extends Resource
{
    protected $category = 'supply_chain';

    public function confirmPackageShipment($warehouse_provider_id, $package)
    {
        return $this->call('POST', 'packages/sync', [
            RequestOptions::JSON => [
                'warehouse_provider_id' => $warehouse_provider_id,
                'package' => $package,
            ]
        ]);
    }
}
