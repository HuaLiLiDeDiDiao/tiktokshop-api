<?php


namespace TiktokShop\Resources;

use TiktokShop\Resource;

class Seller extends Resource
{
    protected $category = 'seller';

    public function getActiveShopList()
    {
        return $this->call('GET', 'shops');
    }

    public function getSellerPermissions()
    {
        return $this->call('GET', 'permissions');
    }
}
